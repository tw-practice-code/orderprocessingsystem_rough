package com.thoughtworks.practice.manager;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Item;

//Manage the inventory operations
public class InventoryManager {

    public boolean isItemPresent(Item item, Inventory inventory) {
        return inventory.getAvailableStock().contains(item);
    }

    public void updateRemainingQuantity(Item orderedItem, Inventory inventory) {
        int index = inventory.getItemIndex(orderedItem);
        Item itemInStock = inventory.getItemFromStock(orderedItem);
        itemInStock.setQuantity(getUpdatedQuantity(orderedItem, itemInStock));
        inventory.getAvailableStock().set(index, itemInStock);
    }

    private int getUpdatedQuantity(Item orderedItem, Item itemInStock) {
        return itemInStock.getQuantity() - orderedItem.getQuantity();
    }

    public boolean isItemPresentInSufficientQuantity(Item orderedItem, Inventory inventory) {
        if (isItemPresent(orderedItem, inventory))
            return validateQuantity(orderedItem, inventory);
        return false;
    }

    private boolean validateQuantity(Item item, Inventory inventory) {
        return item.getQuantity() <= inventory.getItemFromStock(item).getQuantity();
    }
}
