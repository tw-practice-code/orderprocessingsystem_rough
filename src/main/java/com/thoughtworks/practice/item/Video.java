package com.thoughtworks.practice.item;

//Model the item Video
public class Video extends Item {

    private static final int VIDEO_COPIES = 1;

    public Video(String name, int ID) {
        super(name, ID, VIDEO_COPIES);
    }

    public Video(String name, int ID, double cost) {
        super(name, ID, 1, cost);
    }
}
