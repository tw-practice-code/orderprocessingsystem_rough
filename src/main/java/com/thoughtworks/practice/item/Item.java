package com.thoughtworks.practice.item;

import java.util.Objects;

//Model the items in stock
public class Item {
    private String name;
    private int ID;
    private int quantity;
    private double cost;

    public Item(String name, int ID, int quantity) {
        this.name = name;
        this.ID = ID;
        this.quantity = quantity;
    }

    public Item(String name, int ID, int quantity, double cost) {
        this(name, ID, quantity);
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return ID;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return ID == item.ID &&
                name.equals(item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getCost() {
        return cost;
    }
}
