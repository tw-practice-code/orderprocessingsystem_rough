package com.thoughtworks.practice.item;

import java.time.LocalDate;

//Model the customer request to be a member of the system
public class Membership extends Item {
    LocalDate expiry;
    private static final int MEMBERSHIP_COPIES = 1;

    public Membership(String name, int ID) {
        super(name, ID, MEMBERSHIP_COPIES);
        expiry = LocalDate.now().plusMonths(1);
    }

    public Membership(String name, int ID, double cost) {
        super(name, ID, 1, cost);
        expiry = LocalDate.now().plusMonths(1);
    }

    public LocalDate getExpiry() {
        return expiry;
    }
}
