package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.service.ItemService;

import java.util.ArrayList;
import java.util.List;

//Model the order items
public class Order {

    public List<Item> getItemsOfTypeMembership(List<Item> orderedItems) {
        List<Item> itemsOfGivenType = new ArrayList<>();
        for (Item item : orderedItems)
            if (item instanceof Membership)
                itemsOfGivenType.add(item);
        return itemsOfGivenType;
    }

    public double getTotalCostPerOrder(List<Item> orderedItems) {
        double totalCost = 0;
        for (Item item : orderedItems)
            totalCost += new ItemService().getCostPerItem(item);
        return totalCost;
    }

    public int getTotalNumberOfCopiesPerOrder(List<Item> orderedItems) {
        int totalCopies = 0;
        for (Item item : orderedItems)
            totalCopies += item.getQuantity();
        return totalCopies;
    }
}
