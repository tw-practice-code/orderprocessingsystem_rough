package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.InventoryManager;

import java.util.List;

//Validates an order
public class OrderValidator {

    public boolean validate(List<Item> orderedItems, Inventory inventory) {
        if (!isMembershipCountValid(orderedItems)) return false;
        for (Item item : orderedItems)
            if (isItemUnAvailable(item, inventory)) return false;
        return true;
    }

    private boolean isMembershipCountValid(List<Item> orderedItems) {
        int membershipCount = new Order().getItemsOfTypeMembership(orderedItems).size();
        return membershipCount <= 1;
    }

    private boolean isItemUnAvailable(Item item, Inventory inventory) {
        return !new InventoryManager().isItemPresentInSufficientQuantity(item, inventory);
    }
}
