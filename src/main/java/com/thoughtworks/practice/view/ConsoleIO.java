package com.thoughtworks.practice.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Model the input output
public class ConsoleIO {
    private Scanner scanner = new Scanner(System.in);

    public String takeStringInput() {
        return scanner.nextLine();
    }

    public List<String> takeMultiLineInput() {
        List<String> inputOrder = new ArrayList<>();
        while (true) {
            inputOrder.add(takeStringInput());
            display("Do you want to add more items?(y/n)");
            char addMoreItems = scanner.nextLine().charAt(0);
            if (addMoreItems == 'n' || addMoreItems == 'N')
                break;
        }
        return inputOrder;
    }

    public void display(String message) {
        System.out.println(message);
    }

    public boolean isAnotherOrderRequested() {
        display("Do you want to place another order?(y/n)");
        char orderRequested = scanner.next().charAt(0);
        return orderRequested == 'y' || orderRequested == 'Y';
    }
}
