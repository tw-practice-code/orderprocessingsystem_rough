package com.thoughtworks.practice.view;

import com.thoughtworks.practice.controller.OrderController;
import com.thoughtworks.practice.handler.DetailInput;
import com.thoughtworks.practice.handler.PaymentOutput;
import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.PaymentParser;

import java.util.List;

//Model the payment view
public class PaymentUI {
    private PaymentParser paymentParser = new PaymentParser();

    public void paymentSlip(Inventory inventory) {
        List<String> userInput = new DetailInput().getInput();
        List<Item> parsedInput = paymentParser.parseInput(userInput);
        List<Item> processedItems = new OrderController().getProcessedOrderItems(parsedInput, inventory);
        List<String> parsedOutput = paymentParser.parseOutput(processedItems);
        new PaymentOutput().displayPaymentSlip(parsedOutput, parsedInput, inventory);
    }
}
