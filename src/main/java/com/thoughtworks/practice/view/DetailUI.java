package com.thoughtworks.practice.view;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.controller.OrderController;
import com.thoughtworks.practice.handler.DetailInput;
import com.thoughtworks.practice.handler.DetailOutput;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.DetailParser;

import java.util.List;

//Model the detail view
public class DetailUI {
    private OrderController orderController = new OrderController();
    private DetailParser detailParser = new DetailParser();

    public void orderDetails(Inventory inventory) {
        List<String> userInput = new DetailInput().getInput();
        List<Item> parsedInput = detailParser.parseInput(userInput);
        List<Item> processedItems = orderController.getProcessedOrderItems(parsedInput, inventory);
        List<String> parsedOutput = detailParser.parseOutput(processedItems);
        new DetailOutput().displayOutput(parsedOutput, parsedInput, inventory);
    }
}