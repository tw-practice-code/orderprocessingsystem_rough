package com.thoughtworks.practice.handler;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.controller.OrderController;
import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.view.ConsoleIO;

import java.util.List;

//Handle the output for payments
public class PaymentOutput {
    private ConsoleIO consoleIO = new ConsoleIO();

    public void displayPaymentSlip(List<String> outputList, List<Item> orderedItems, Inventory inventory) {
        if (outputList == null)
            displayUnProcessedOrder(orderedItems, inventory);
        else
            displayPaymentSlipForProcessedOrder(outputList, orderedItems);
    }

    private void displayUnProcessedOrder(List<Item> orderedItems, Inventory inventory) {
        String unSuccessReason = new OrderController().getReasonForUnprocessedOrder(orderedItems, inventory);
        consoleIO.display(String.format("Can't process the order as - %1$s", unSuccessReason));
    }

    private void displayPaymentSlipForProcessedOrder(List<String> outputList, List<Item> orderedItems) {
        String format = "| %1$-18s| %2$-8s| %3$-13s| %4$-10s| %5$-18s|";
        displayTitleSlip(format);
        displayOrderItems(outputList, format);
        displayTotalAmount(orderedItems, format);
    }

    private void displayOrderItems(List<String> outputList, String format) {
        for (String output : outputList) {
            String[] outputItem = output.split(" ");
            displayAllItems(outputItem, format);
        }
    }

    private void displayAllItems(String[] outputItem, String format) {
        consoleIO.display(String.format(format, outputItem[0], outputItem[1], outputItem[2], outputItem[3], outputItem[4]));
    }

    private void displayTitleSlip(String format) {
        consoleIO.display("\nYour Payment Slip:");
        consoleIO.display("------------------------------------------------------------------------------");
        consoleIO.display(String.format(format, "ITEM", "ID", "COST/ITEM", "QUANTITY", "TOTAL COST (INR)"));
        consoleIO.display("------------------------------------------------------------------------------");

    }

    private void displayTotalAmount(List<Item> acceptedOrder, String format) {
        consoleIO.display("------------------------------------------------------------------------------");
        consoleIO.display(String.format(format, "TOTAL", "", "", Integer.toString(new Order().getTotalNumberOfCopiesPerOrder(acceptedOrder)), Double.toString(new Order().getTotalCostPerOrder(acceptedOrder))));
        consoleIO.display("------------------------------------------------------------------------------");
    }
}
