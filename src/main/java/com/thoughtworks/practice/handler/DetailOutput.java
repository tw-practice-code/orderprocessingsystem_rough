package com.thoughtworks.practice.handler;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.controller.OrderController;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.view.ConsoleIO;

import java.util.List;

//Handle the output for order details
public class DetailOutput {
    private ConsoleIO consoleIO = new ConsoleIO();

    public void displayOutput(List<String> outputList, List<Item> orderedItems, Inventory inventory) {
        if (outputList != null)
            displayProcessedOrder(outputList);
        else
            displayUnProcessedOrder(orderedItems, inventory);
    }

    private void displayUnProcessedOrder(List<Item> orderedItems, Inventory inventory) {
        String unSuccessReason = new OrderController().getReasonForUnprocessedOrder(orderedItems, inventory);
        consoleIO.display(String.format("Can't process the order as - %1$s", unSuccessReason));
    }

    private void displayProcessedOrder(List<String> outputList) {
        for (String output : outputList) {
            String[] outputItem = output.split(" ");
            displayAllItems(outputItem);
        }
    }

    private void displayAllItems(String[] outputItem) {
        if (outputItem[0].equals("BOOK"))
            consoleIO.display(String.format("%1$s, ID %2$s, Quantity %3$s", outputItem[0], outputItem[1], outputItem[2]));
        else if (outputItem[0].equals("VIDEO"))
            consoleIO.display(String.format("%1$s, ID %2$s", outputItem[0], outputItem[1]));
        else
            consoleIO.display(String.format("%1$s, ID %2$s, Expires on: %3$s", outputItem[0], outputItem[1], outputItem[2]));
    }
}