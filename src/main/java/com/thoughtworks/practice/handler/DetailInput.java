package com.thoughtworks.practice.handler;

import com.thoughtworks.practice.view.ConsoleIO;

import java.util.List;

//Handle the input for order details
public class DetailInput {
    private ConsoleIO consoleIO = new ConsoleIO();

    public List<String> getInput() {
        consoleIO.display(getOrderStart());
        List<String> inputList = consoleIO.takeMultiLineInput();
        consoleIO.display(getOrderEnd());
        return inputList;
    }

    String getOrderStart() {
        return "ORDER START";
    }

    String getOrderEnd() {
        return "ORDER END";
    }
}
