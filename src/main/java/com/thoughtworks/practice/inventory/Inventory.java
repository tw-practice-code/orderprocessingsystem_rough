package com.thoughtworks.practice.inventory;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.DetailParser;

import java.util.ArrayList;
import java.util.List;

//Model the list of items in the stock
public class Inventory {
    private List<Item> availableStock;

    public Inventory() {
        availableStock = new ArrayList<>();
    }

    public void load(String filePath) {
        FileReader fileReader = new FileReader();
        List<String> stock = fileReader.readFromFile(filePath);
        availableStock = new DetailParser().parseInput(stock);
    }

    public List<Item> getAvailableStock() {
        return availableStock;
    }

    public int getItemIndex(Item item) {
        return availableStock.indexOf(item);
    }

    public Item getItemFromStock(Item item) {
        return availableStock.get(getItemIndex(item));
    }
}
