package com.thoughtworks.practice.inventory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Read the Stock from a given file
public class FileReader {
    private List<String> fileContent;

    public FileReader() {
        fileContent = new ArrayList<>();
    }

    List<String> readFromFile(String filePath) {
        Scanner scanner = null;
        try {
            scanner = getFile(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert scanner != null;
        getData(scanner);
        return fileContent;
    }

    private void getData(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            fileContent.add(nextLine);
        }
    }

    Scanner getFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        return new Scanner(file);
    }
}