package com.thoughtworks.practice;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.InventoryManager;

import java.util.List;

//Model the system that process multiple orders
public class OrderProcessingSystem {
    private InventoryManager inventoryManager = new InventoryManager();

    public void placeOrder(List<Item> orderedItems, Inventory inventory) {
        for (Item item : orderedItems)
            inventoryManager.updateRemainingQuantity(item, inventory);
    }
}
