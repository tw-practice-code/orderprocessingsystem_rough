package com.thoughtworks.practice;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.view.ConsoleIO;
import com.thoughtworks.practice.view.DetailUI;

//Start the application
public class Main {
    private static Inventory inventory = new Inventory();
    private static DetailUI detailUI = new DetailUI();
    private static ConsoleIO consoleIO = new ConsoleIO();

    public static void main(String[] args) {
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
        do {
            detailUI.orderDetails(inventory);
        } while (consoleIO.isAnotherOrderRequested());
    }
}
