package com.thoughtworks.practice.controller;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.OrderService;

import java.util.List;

//Fetch the processed order
public class OrderController {

    public List<Item> getProcessedOrderItems(List<Item> orderedItems, Inventory inventory) {
        OrderService orderService = new OrderService();
        return orderService.process(orderedItems, inventory);
    }

    public String getReasonForUnprocessedOrder(List<Item> orderedItems, Inventory inventory) {
        return new OrderService().getReasonsForUnSuccessfulProcessing(orderedItems, inventory);
    }
}
