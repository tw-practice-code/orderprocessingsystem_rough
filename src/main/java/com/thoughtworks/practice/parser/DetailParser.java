package com.thoughtworks.practice.parser;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.item.Video;

import java.util.ArrayList;
import java.util.List;

//Parse the items in stock
public class DetailParser {

    public List<Item> parseInput(List<String> inputList) {
        List<Item> items = new ArrayList<>();
        for (String input : inputList) {
            String[] inputDetails = input.split(" ");
            items.add(getItem(inputDetails));
        }
        return items;
    }

    Item getItem(String[] inputDetails) {
        if (inputDetails[0].equals("BOOK")) {
            return new Book(inputDetails[0], Integer.parseInt(inputDetails[1]), Integer.parseInt(inputDetails[2]));
        } else if (inputDetails[0].equals("VIDEO"))
            return new Video(inputDetails[0], Integer.parseInt(inputDetails[1]));
        return new Membership(inputDetails[0], Integer.parseInt(inputDetails[1]));
    }

    public List<String> parseOutput(List<Item> outputList) {
        if (outputList == null)
            return null;
        List<String> parsedOutput = new ArrayList<>();
        fetchOutputString(outputList, parsedOutput);
        return parsedOutput;
    }

    private void fetchOutputString(List<Item> outputList, List<String> parsedOutput) {
        for (Item item : outputList) {
            if (item instanceof Book)
                parsedOutput.add(item.getName() + " " + item.getID() + " " + item.getQuantity());
            else if (item instanceof Video)
                parsedOutput.add(item.getName() + " " + item.getID());
            else
                parsedOutput.add(item.getName() + " " + item.getID() + " " + ((Membership) item).getExpiry());
        }
    }
}
