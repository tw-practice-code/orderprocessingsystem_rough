package com.thoughtworks.practice.parser;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.service.ItemService;

import java.util.ArrayList;
import java.util.List;

//Parse the items for generating payment slip.
public class PaymentParser {

    private static final int MEMBERSHIP_COST = 200;
    private static final int VIDEO_COST = 50;
    private static final int BOOK_COST = 100;

    Item getItem(String[] inputDetails) {
        if (inputDetails[0].equals("BOOK")) {
            return new Book(inputDetails[0], Integer.parseInt(inputDetails[1]), Integer.parseInt(inputDetails[2]), BOOK_COST);
        } else if (inputDetails[0].equals("VIDEO"))
            return new Video(inputDetails[0], Integer.parseInt(inputDetails[1]), VIDEO_COST);
        return new Membership(inputDetails[0], Integer.parseInt(inputDetails[1]), MEMBERSHIP_COST);
    }

    public List<Item> parseInput(List<String> inputList) {
        List<Item> items = new ArrayList<>();
        for (String input : inputList) {
            String[] inputDetails = input.split(" ");
            items.add(getItem(inputDetails));
        }
        return items;
    }

    public List<String> parseOutput(List<Item> outputList) {
        if (outputList == null)
            return null;
        return fetchOutputString(outputList);
    }

    private List<String> fetchOutputString(List<Item> outputList) {
        List<String> parsedOutput = new ArrayList<>();
        for (Item item : outputList)
            parsedOutput.add(item.getName() + " " + item.getID() + " " + item.getQuantity() + " " + item.getCost() + " " + new ItemService().getCostPerItem(item));
        return parsedOutput;
    }
}
