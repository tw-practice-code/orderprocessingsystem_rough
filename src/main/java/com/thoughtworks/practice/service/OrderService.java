package com.thoughtworks.practice.service;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.OrderProcessingSystem;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.exception.InsufficientQuantityException;
import com.thoughtworks.practice.service.exception.ItemNotFoundException;
import com.thoughtworks.practice.validator.OrderValidator;

import java.util.List;

//Process the order
public class OrderService {
    private OrderValidator orderValidator = new OrderValidator();

    public List<Item> process(List<Item> orderedItems, Inventory inventory) {
        if (orderValidator.validate(orderedItems, inventory)) {
            new OrderProcessingSystem().placeOrder(orderedItems, inventory);
            return orderedItems;
        } else
            return null;
    }

    public String getReasonsForUnSuccessfulProcessing(List<Item> orderedItems, Inventory inventory) {
        Reason reason = new Reason();
        try {
            reason.find(orderedItems, inventory);
        } catch (ItemNotFoundException e) {
            return e.getItemNotFoundExceptionMessage();
        } catch (InsufficientQuantityException e) {
            return e.getInsufficientQuantityExceptionMessage();
        }
        return "Membership Request is Invalid";
    }
}
