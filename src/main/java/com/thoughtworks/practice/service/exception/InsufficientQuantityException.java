package com.thoughtworks.practice.service.exception;

//Exception when item is present but in insufficient quantity
public class InsufficientQuantityException extends Throwable {

    public String getInsufficientQuantityExceptionMessage() {
        return "The requested item is not available in stock.";
    }
}
