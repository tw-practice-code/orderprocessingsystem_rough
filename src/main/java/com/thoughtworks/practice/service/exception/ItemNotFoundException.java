package com.thoughtworks.practice.service.exception;

//Exception when item is not present in stock
public class ItemNotFoundException extends Throwable {

    public String getItemNotFoundExceptionMessage() {
        return "We do not sell the following item.";
    }
}
