package com.thoughtworks.practice.service;

import com.thoughtworks.practice.item.Item;

//Provide services associated with an item
public class ItemService {

    double getCostOfOneItem(Item item) {
        return item.getCost();
    }

    int getNumberOfCopies(Item item) {
        return item.getQuantity();
    }

    public double getCostPerItem(Item item) {
        return getCostOfOneItem(item) * getNumberOfCopies(item);
    }
}
