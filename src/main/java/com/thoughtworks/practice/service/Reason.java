package com.thoughtworks.practice.service;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.InventoryManager;
import com.thoughtworks.practice.service.exception.InsufficientQuantityException;
import com.thoughtworks.practice.service.exception.ItemNotFoundException;

import java.util.List;

//Return the reasons for unsuccessful processing
class Reason {

    public void find(List<Item> orderedItems, Inventory inventory) throws ItemNotFoundException, InsufficientQuantityException {
        for (Item item : orderedItems)
            if (isItemNotPresentInStock(inventory, item))
                throw new ItemNotFoundException();
            else if (isItemQuantityInsufficient(inventory, item))
                throw new InsufficientQuantityException();
    }

    private boolean isItemQuantityInsufficient(Inventory inventory, Item item) {
        return !new InventoryManager().isItemPresentInSufficientQuantity(item, inventory);
    }

    private boolean isItemNotPresentInStock(Inventory inventory, Item item) {
        return !new InventoryManager().isItemPresent(item, inventory);
    }
}
