package com.thoughtworks.practice.inventory;

import com.thoughtworks.practice.item.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class InventoryTest {
    private Inventory inventory;

    @BeforeEach
    void setup() {
        inventory = mock(Inventory.class);
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }

    @Test
    void shouldGiveTheAvailableStock() {
        List<Item> stock = inventory.getAvailableStock();
        verify(inventory).getAvailableStock();
    }
}