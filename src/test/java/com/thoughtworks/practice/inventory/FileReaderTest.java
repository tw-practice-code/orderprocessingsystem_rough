package com.thoughtworks.practice.inventory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FileReaderTest {
    private FileReader fileReader;

    @BeforeEach
    void setup() {
        fileReader = new FileReader();
    }

    @Test
    void shouldThrowExceptionWhenFileDoesNotExist() {
        String filePath = "";
        assertThrows(FileNotFoundException.class, () -> fileReader.getFile(filePath));
    }

    @Test
    void shouldGiveStockWhenReadFromFile() {
        String filepath = "./src/main/java/com/thoughtworks/practice/resource/Stock.txt";
        List<String> expected = Arrays.asList("BOOK 1 5", "BOOK 2 5", "VIDEO 1", "VIDEO 2", "MEMBERSHIP 101", "MEMBERSHIP 102");
        assertEquals(expected, fileReader.readFromFile(filepath));
    }
}
