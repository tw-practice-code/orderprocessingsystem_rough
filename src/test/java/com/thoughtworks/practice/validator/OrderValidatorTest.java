package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderValidatorTest {
    private OrderValidator orderValidator;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderValidator = new OrderValidator();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }

    @Test
    void shouldBeFalseWhenItemNotPresentInStock() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 11, 2), new Video("VIDEO", 1));
        assertFalse(orderValidator.validate(orderedItems, inventory));
    }

    @Test
    void shouldBeTrueWhenItemIsPresentInStock() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        assertTrue(orderValidator.validate(orderedItems, inventory));
    }
}
