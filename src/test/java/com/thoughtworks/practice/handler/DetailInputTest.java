package com.thoughtworks.practice.handler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DetailInputTest {
    private DetailInput detailInput;

    @BeforeEach
    void setup() {
        detailInput = new DetailInput();
    }

    @Test
    void shouldGetOrderStart() {
        assertEquals("ORDER START", detailInput.getOrderStart());
    }

    @Test
    void shouldGetOrderEnd() {
        assertEquals("ORDER END", detailInput.getOrderEnd());
    }
}
