package com.thoughtworks.practice.parser;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PaymentParserTest {
    private PaymentParser paymentParser;

    @BeforeEach
    void setup() {
        paymentParser = new PaymentParser();
    }

    @Test
    void shouldParseStringBook() {
        String[] stockDetails = {"BOOK", "1", "2", "100"};
        assertEquals(new Book("BOOK", 1, 2, 100), (paymentParser.getItem(stockDetails)));
    }

    @Test
    void shouldParseStringVideo() {
        String[] stockDetails = {"VIDEO", "1", "50"};
        assertEquals(new Video("VIDEO", 1, 50), (paymentParser.getItem(stockDetails)));
    }

    @Test
    void shouldParseStringMembership() {
        String[] stockDetails = {"MEMBERSHIP", "101", "200"};
        assertEquals(new Video("MEMBERSHIP", 101, 200), (paymentParser.getItem(stockDetails)));
    }

    @Test
    void shouldGiveItemsInStock() {
        List<String> stockList = Arrays.asList("BOOK 1 2", "VIDEO 1");
        List<Item> expected = Arrays.asList(new Book("BOOK", 1, 2, 100), new Video("VIDEO", 1, 50));
        assertEquals(expected, paymentParser.parseInput(stockList));
    }

    @Test
    void shouldGiveStringOutputForGivenItems() {
        List<Item> items = Arrays.asList(new Book("BOOK", 1, 2, 100), new Video("VIDEO", 1, 50));
        List<String> expected = Arrays.asList("BOOK 1 2 100 200", "VIDEO 1 1 50 50");
        assertEquals(expected, paymentParser.parseOutput(items));
    }
}
