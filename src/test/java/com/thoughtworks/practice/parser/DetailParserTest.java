package com.thoughtworks.practice.parser;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DetailParserTest {
    private DetailParser detailParser;

    @BeforeEach
    void setup() {
        detailParser = new DetailParser();
    }

    @Test
    void shouldParseStringBook() {
        String[] stockDetails = {"BOOK", "1", "2"};
        assertEquals(new Book("BOOK", 1, 2), (detailParser.getItem(stockDetails)));
    }

    @Test
    void shouldParseStringVideo() {
        String[] stockDetails = {"VIDEO", "1"};
        assertEquals(new Video("VIDEO", 1), (detailParser.getItem(stockDetails)));
    }

    @Test
    void shouldGiveItemsInStock() {
        List<String> stockList = Arrays.asList("BOOK 1 2", "VIDEO 1");
        List<Item> expected = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        assertEquals(expected, detailParser.parseInput(stockList));
    }

    @Test
    void shouldGiveStringOutputForGivenItems() {
        List<Item> items = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        List<String> expected = Arrays.asList("BOOK 1 2", "VIDEO 1");
        assertEquals(expected, detailParser.parseOutput(items));
    }
}
