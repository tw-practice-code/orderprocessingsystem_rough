package com.thoughtworks.practice.item;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MembershipTest {

    @Test
    void shouldGiveQuantityOfMembership() {
        Item membership = new Membership("MEMBERSHIP", 101);
        assertEquals(1, membership.getQuantity());
    }

    @Test
    void shouldGiveCustomerIDAssociatedWithMembership() {
        Item membership = new Membership("MEMBERSHIP", 101);
        assertEquals(101, membership.getID());
    }

    @Test
    void shouldGetTheExpiryDateOfMembership() {
        Membership membership = new Membership("MEMBERSHIP", 101);
        assertEquals(LocalDate.now().plusMonths(1), membership.getExpiry());
    }
}
