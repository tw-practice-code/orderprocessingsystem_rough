package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderTest {
    private Order order;

    @BeforeEach
    void setup() {
        order = new Order();
    }

    @Test
    void shouldGiveItemsOfTypeMembership() {
        Item item1 = new Membership("MEMBERSHIP", 101);
        Item item2 = new Membership("MEMBERSHIP", 102);
        List<Item> orderedItems = Arrays.asList(item1, item2);
        assertEquals(2, order.getItemsOfTypeMembership(orderedItems).size());
    }

    @Test
    void shouldGiveTotalCostOfTheOrder() {
        Item firstItem = new Membership("MEMBERSHIP", 101, 200);
        Item secondItem = new Book("BOOK", 1, 2, 100);
        Item thirdItem = new Video("VIDEO", 1, 50);
        List<Item> orderedItems = Arrays.asList(firstItem, secondItem, thirdItem);
        assertEquals(450, order.getTotalCostPerOrder(orderedItems));
    }

    @Test
    void shouldGiveTotalNumberOfCopiesPerOrder() {
        Item firstItem = new Membership("MEMBERSHIP", 101, 200);
        Item secondItem = new Book("BOOK", 1, 2, 100);
        Item thirdItem = new Video("VIDEO", 1, 50);
        List<Item> orderedItems = Arrays.asList(firstItem, secondItem, thirdItem);
        assertEquals(4, order.getTotalNumberOfCopiesPerOrder(orderedItems));
    }
}
