package com.thoughtworks.practice;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderProcessingSystemTest {
    private OrderProcessingSystem orderProcessingSystem;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderProcessingSystem = new OrderProcessingSystem();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }

    @Test
    void shouldUpdatedQuantityWhenAValidOrderIsPlaced() {
        Item book = new Book("BOOK", 1, 2);
        List<Item> orderedItems = Collections.singletonList(book);
        orderProcessingSystem.placeOrder(orderedItems, inventory);
        assertEquals(3, inventory.getItemFromStock(book).getQuantity());
    }
}
