package com.thoughtworks.practice.controller;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrderControllerTest {

    private OrderController orderController;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderController = new OrderController();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }

    @Test
    void shouldFetchTheProcessedOrder() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        assertEquals(orderedItems, orderController.getProcessedOrderItems(orderedItems, inventory));
    }

    @Test
    void shouldGiveReasonWhenOrderedItemIsNotPresentInStock() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 11, 2));
        assertEquals("We do not sell the following item.", orderController.getReasonForUnprocessedOrder(orderedItems, inventory));
    }
}