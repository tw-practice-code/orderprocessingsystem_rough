package com.thoughtworks.practice.manager;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryManagerTest {
    private InventoryManager inventoryManager;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        inventoryManager = new InventoryManager();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }


    @Test
    void shouldBeTrueWhenItemIsPresentInStock() {
        assertTrue(inventoryManager.isItemPresent(new Book("BOOK", 1, 2), inventory));
    }

    @Test
    void shouldBeFalseWhenItemIsNotPresentInStock() {
        assertFalse(inventoryManager.isItemPresent(new Book("BOOK", 11, 2), inventory));
    }

    @Test
    void shouldUpdateRemainingQuantityOfAnOrderedItem() {
        Item item = new Book("BOOK", 1, 3);
        inventoryManager.updateRemainingQuantity(item, inventory);
        assertEquals(2, inventory.getItemFromStock(item).getQuantity());
    }

    @Test
    void shouldBeTrueWhenItemIsPresentInSufficientQuantity() {
        Item item = new Book("BOOK", 1, 3);
        assertTrue(inventoryManager.isItemPresentInSufficientQuantity(item, inventory));
    }

    @Test
    void shouldBeFalseWhenItemIsNotPresentInSufficientQuantity() {
        Item item = new Book("BOOK", 1, 8);
        assertFalse(inventoryManager.isItemPresentInSufficientQuantity(item, inventory));
    }
}
