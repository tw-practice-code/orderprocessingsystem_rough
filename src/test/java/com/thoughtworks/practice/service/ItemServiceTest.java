package com.thoughtworks.practice.service;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemServiceTest {
    private ItemService itemService;

    @BeforeEach
    void setup() {
        itemService = new ItemService();
    }

    @Test
    void shouldGiveCostOfOneItem() {
        Item item = new Book("BOOK", 1, 2, 100);
        assertEquals(100, itemService.getCostOfOneItem(item));
    }

    @Test
    void shouldGiveNumberOfCopiesOfAnItem() {
        Item item = new Book("BOOK", 1, 2, 100);
        assertEquals(2, itemService.getNumberOfCopies(item));
    }

    @Test
    void shouldGiveTotalCostPerItem() {
        Item item = new Book("BOOK", 1, 3, 100);
        assertEquals(300, itemService.getCostPerItem(item));
    }
}
