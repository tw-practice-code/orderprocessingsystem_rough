package com.thoughtworks.practice.service.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItemNotFoundExceptionTest {

    @Test
    void shouldGiveExceptionMessageWhenItemNotFound() {
        ItemNotFoundException itemNotFoundException = new ItemNotFoundException();
        assertEquals("We do not sell the following item.", itemNotFoundException.getItemNotFoundExceptionMessage());
    }
}
