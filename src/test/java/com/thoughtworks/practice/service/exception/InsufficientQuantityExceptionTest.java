package com.thoughtworks.practice.service.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InsufficientQuantityExceptionTest {

    @Test
    void shouldGiveExceptionMessageWhenItemIsInsufficientInQuantity() {
        InsufficientQuantityException insufficientQuantityException = new InsufficientQuantityException();
        assertEquals("The requested item is not available in stock.", insufficientQuantityException.getInsufficientQuantityExceptionMessage());
    }
}
