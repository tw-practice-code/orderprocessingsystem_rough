package com.thoughtworks.practice.service;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.exception.InsufficientQuantityException;
import com.thoughtworks.practice.service.exception.ItemNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ReasonTest {
    private Reason reason;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        reason = new Reason();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }

    @Test
    void shouldThrowExceptionWhenItemIsNotFound() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 90, 2));
        assertThrows(ItemNotFoundException.class, () -> reason.find(orderedItems, inventory));
    }

    @Test
    void shouldNotThrowExceptionWhenItemIsFound() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 2));
        assertDoesNotThrow(() -> reason.find(orderedItems, inventory));
    }

    @Test
    void shouldThrowExceptionWhenItemIsPresentInInsufficientQuantity() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 20));
        assertThrows(InsufficientQuantityException.class, () -> reason.find(orderedItems, inventory));
    }

    @Test
    void shouldNotThrowExceptionWhenItemIsPresentInSufficientQuantity() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 2));
        assertDoesNotThrow(() -> reason.find(orderedItems, inventory));
    }
}
