package com.thoughtworks.practice.service;

import com.thoughtworks.practice.inventory.Inventory;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderServiceTest {
    private OrderService orderService;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderService = new OrderService();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/resource/Stock.txt");
    }

    @Test
    void shouldGiveProcessedOrder() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        assertEquals(orderedItems, orderService.process(orderedItems, inventory));
    }

    @Test
    void shouldBeNullWhenOrderCannotBeProcessed() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 11, 2), new Video("VIDEO", 1));
        assertEquals(null, orderService.process(orderedItems, inventory));
    }

    @Test
    void shouldGetExceptionMessageWhenItemIsNotPresentInStock() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 11, 3));
        assertEquals("We do not sell the following item.", orderService.getReasonsForUnSuccessfulProcessing(orderedItems, inventory));
    }

    @Test
    void shouldGetExceptionMessageWhenItemIsPresentInInsufficientQuantity() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 30));
        assertEquals("The requested item is not available in stock.", orderService.getReasonsForUnSuccessfulProcessing(orderedItems, inventory));
    }
}
